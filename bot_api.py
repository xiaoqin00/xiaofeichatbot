#!/usr/bin/env python
# coding: utf-8

from chatterbot import ChatBot
from chatterbot.trainers import UbuntuCorpusTrainer
import hug


bot = ChatBot("deepThought", #bot = ChatBot("deepThought", read_only=True)
storage_adapter="chatterbot.storage.MongoDatabaseAdapter", #Mongo数据库适配器
logic_adapters=["chatterbot.logic.BestMatch"], #最佳匹配的适配器
filters=["chatterbot.filters.RepetitiveResponseFilter"], #过滤器，防止聊天机器人重复陈述
database="chatterbot-database" #数据库的名称
)


@hug.get()
def get_response(user_input):
    response = bot.get_response(user_input).text
    return {"response":response}
